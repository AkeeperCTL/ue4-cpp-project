﻿//#include "CStack.h"
#include <iostream>
#include "CAnimal.h"

int main()
{
	setlocale(0, "RU");
	
	const int animalsMax = 4;
	CAnimal* animals[animalsMax] = {nullptr};

	CAnimal* dogAnimal = new CDog();
	CAnimal* cowAnimal = new CCow();
	CAnimal* catAnimal = new CCat();
	CAnimal* birdAnimal = new CBird();

	animals[0] = dogAnimal;
	animals[1] = cowAnimal;
	animals[2] = catAnimal;
	animals[3] = birdAnimal;

	for (auto& animal : animals)
	{
		animal->Voice();
	}

	for (auto& animal : animals)
	{
		delete animal;
	}
}