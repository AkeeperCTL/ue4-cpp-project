#pragma once

#include <iostream>

typedef std::string string;

template<typename Type> class CStack
{
public:
	CStack(int _size)
	{
		m_enteredSize = _size;
		m_arrayElements = new Type[_size];

		for (int i = 0; i < m_enteredSize; i++)
			m_arrayElements[i] = 0;
	}

	~CStack()
	{
		delete[] m_arrayElements;
		m_arrayElements = nullptr;
	}

	//Adds element to the stack
	void push(Type element)
	{
		m_arrayElements[m_nextElementIndex] = element;
		m_nextElementIndex++;
	}

	//Returns the peek value in the stack and delete them from this stack
	Type pop()
	{
		if (m_nextElementIndex != 0)
		{
			int currentElementIndex = m_nextElementIndex - 1;
			Type result = m_arrayElements[currentElementIndex];

			m_arrayElements[m_nextElementIndex] = NULL;
			m_nextElementIndex--;

			return result;
		}

		std::cout << "���� ����, pop ���������� ��������� shit" << std::endl;
		return 0;
	}

	//Returns the peek value in the stack but not delete them
	Type peek()
	{
		if (m_nextElementIndex != 0)
		{
			int currentElementIndex = m_nextElementIndex - 1;
			Type result = m_arrayElements[currentElementIndex];

			return result;
		}

		std::cout << "���� ����, peek ���������� ��������� shit" << std::endl;
		return 0;
	}

	//Returns the count of stack elements
	int count()
	{
		return m_nextElementIndex;
	}

private:
	int m_enteredSize = 0;
	int m_nextElementIndex = 0;
	Type* m_arrayElements = nullptr;
};

template<> class CStack<string>
{
public:
	CStack(int _size)
	{
		m_enteredSize = _size;
		m_arrayElements = new string[_size];

		for (int i = 0; i < m_enteredSize; i++)
			m_arrayElements[i] = "";
	}

	~CStack()
	{
		delete[] m_arrayElements;
		m_arrayElements = nullptr;
	}

	//Adds element to the stack
	void push(string element)
	{
		m_arrayElements[m_nextElementIndex] = element;
		m_nextElementIndex++;
	}

	//Returns the peek value in the stack and delete them from this stack
	string pop()
	{
		if (m_nextElementIndex != 0)
		{
			int currentElementIndex = m_nextElementIndex - 1;
			string result = m_arrayElements[currentElementIndex];

			m_arrayElements[m_nextElementIndex] = "";
			m_nextElementIndex--;

			return result;
		}

		std::cout << "���� ����, pop ���������� ��������� shit" << std::endl;
		return "";
	}

	//Returns the peek value in the stack but not delete them
	string peek()
	{
		if (m_nextElementIndex != 0)
		{
			int currentElementIndex = m_nextElementIndex - 1;
			string result = m_arrayElements[currentElementIndex];

			return result;
		}

		//std::cout << "���� ����, peek ���������� ��������� shit" << std::endl;
		return "";
	}

	//Returns the count of stack elements
	int count()
	{
		return m_nextElementIndex;
	}

private:
	int m_enteredSize = 0;
	int m_nextElementIndex = 0;
	string* m_arrayElements = nullptr;
};