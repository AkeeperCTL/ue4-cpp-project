#pragma once

class CAnimal
{
public:
	CAnimal()
	{

	}
	
	virtual void Voice();

private:
};

class CDog : public CAnimal
{
public:
	CDog()
	{
		CAnimal();
	}

	void Voice() override;

private:
};

class CCat : public CAnimal
{
public:
	CCat()
	{
		CAnimal();
	}

	void Voice() override;

private:
};

class CCow : public CAnimal
{
public:
	CCow()
	{
		CAnimal();
	}

	void Voice() override;

private:
};

class CBird : public CAnimal
{
public:
	CBird()
	{
		CAnimal();
	}

	void Voice() override;

private:
};